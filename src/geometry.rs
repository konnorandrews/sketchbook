use core::ops::{Add, AddAssign, Mul, MulAssign};

/// 2D point (x, y)
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
pub struct Point2<T> {
    /// x axis distance.
    pub x: T,

    /// y axis distance.
    pub y: T,
}

impl<T> AddAssign for Point2<T>
where
    T: AddAssign<T>,
{
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T> Add for Point2<T>
where
    T: Add<T, Output = T>,
{
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T> Add<&Self> for Point2<T>
where
    T: Add<T, Output = T> + Clone,
{
    type Output = Self;

    fn add(self, rhs: &Self) -> Self {
        Self {
            x: self.x + rhs.x.clone(),
            y: self.y + rhs.y.clone(),
        }
    }
}

impl<T> Add<Self> for &Point2<T>
where
    T: Add<T, Output = T> + Clone,
{
    type Output = Point2<T>;

    fn add(self, rhs: Self) -> Point2<T> {
        Point2 {
            x: self.x.clone() + rhs.x.clone(),
            y: self.y.clone() + rhs.y.clone(),
        }
    }
}

impl<T> AddAssign<&Point2<T>> for Point2<T>
where
    T: AddAssign<T> + Copy,
{
    fn add_assign(&mut self, rhs: &Point2<T>) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T> AddAssign<Vector2<T>> for Point2<T>
where
    T: AddAssign<T> + Copy,
{
    fn add_assign(&mut self, rhs: Vector2<T>) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

/// 2D size (width, height)
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct Size2<T> {
    /// Width component
    pub width: T,

    /// Height component
    pub height: T,
}

/// 2D vector (x, y)
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct Vector2<T> {
    /// x axis magnitude.
    pub x: T,

    /// y axis magnitude.
    pub y: T,
}

impl<T> Mul<T> for Vector2<T>
where
    T: MulAssign<T> + Copy,
{
    type Output = Vector2<T>;

    fn mul(mut self, rhs: T) -> Self {
        self.x *= rhs;
        self.y *= rhs;
        self
    }
}

//! Built-in reams to run sketches.
//!
//! A ream runs sketches inside an environment.

#[cfg(feature = "ream-single")]
mod single;

#[cfg(feature = "ream-parallel")]
mod parallel;

#[cfg(feature = "ream-concurrent")]
mod concurrent;

#[cfg(feature = "ream-parallel")]
pub use parallel::*;

#[cfg(feature = "ream-single")]
pub use single::*;

#[cfg(feature = "ream-concurrent")]
pub use concurrent::*;

use crate::env::*;

#[derive(Debug)]
pub enum ReamError<R, E> {
    Ream(R),
    Env(E),
}

pub type ReamResult<T, O> = Result<O, ReamError<<T as Ream>::Error, ErrorOf<<T as Ream>::Env>>>;

/// Trait for reams.
///
/// A ream runs one or more sketches in the context of an environment.
pub trait Ream {
    /// Environment ream will run in.
    type Env: Environment;

    /// Error the ream can return.
    type Error;

    /// Update all sketches managed by the ream.
    ///
    /// The `mill` is used to create new pages from the environment.
    fn update(&mut self, mill: &mut MillOf<Self::Env>) -> ReamResult<Self, Status>;
}

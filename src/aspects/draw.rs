//! Draw on page.
//!
//! This aspect enables 2D drawing for an environment.
//!
//! ```
//! use sketchbook::aspects::draw;
//! # sketchbook::draw_env!();
//!
//! #[apply(derive_sketch)]
//! #[sketch(env=single_aspect, aspects=(draw))]
//! struct App {
//!     #[page]
//!     page: single_aspect::Page,
//! }
//!
//! impl draw::Handlers for App {
//!     fn draw(&mut self) {
//!         self.background((100, 100, 100));
//!
//!         self.circle((60, 60, 10));
//!         self.rect((40, 80, 80, 100));
//!     }
//! }
//! ```

/// Create test environment for draw aspect for use in doc tests.
#[doc(hidden)]
#[macro_export]
macro_rules! draw_env {
    {} => {
        use sketchbook::derive_sketch;
        use macro_rules_attribute::apply;
        use sketchbook::aspects::draw::SketchExt;
        mod single_aspect {
            use sketchbook::aspects::draw;
            use sketchbook::aspects::draw::*;
            use sketchbook::Size2;

            sketchbook::env_for_aspect!(draw);

            sketchbook::compose! {
                pub enum Events {
                    #[part]
                    Aspect(draw::Event<EnvSpecificMarker>),
                }
            }

            sketchbook::compose! {
                #[derive(Default)]
                pub struct Page {
                    #[part]
                    pub aspect: draw::EnvData<EnvSpecificMarker>,
                }
            }

            impl draw::EnvSpecific for EnvSpecificMarker {
                type Num = f32;
                type Color = (u8, u8, u8);
                type ShapeBuffer = Vec<Shape<f32, (u8, u8, u8)>>;
                fn default_size() -> Size2<Self::Num> { Size2 { width: 0.0, height: 0.0 } }
                fn default_context() -> Context<Self::Num, Self::Color> { Context::default() }
                fn default_background() -> Self::Color { (0, 0, 0) }
                fn default_frame_rate() -> Self::Num { 1.0 }
                fn no_color() -> Self::Color { (0, 0, 0) }
            }
        }
    }
}

mod circle;
mod context;
mod mode;
mod rectangle;
mod shape;

#[cfg(feature = "color")]
mod color;

pub use circle::*;
pub use context::*;
pub use mode::*;
pub use rectangle::*;
pub use shape::*;

#[cfg(feature = "color")]
pub use color::*;

use crate::compose::TryAsPart;
use crate::real::ToReal;
use crate::{geometry::*, Real};

use core::marker::PhantomData;

use crate::{compose::AsPart, Environment, PageOf, Sketch};

/// Event handlers for draw aspect.
///
/// Each handler has a default implementation that does nothing.
/// Implement a method to perform actions when the associated event happens.
pub trait Handlers
where
    Self: Sketch,
    Self::Env: AssociatedEnvSpecificMarker,
{
    /// Handler for [`Event::Draw`] event.
    #[inline]
    fn draw(&mut self) {
        // by default do nothing
    }
}

/// Extension methods for sketch.
///
/// These methods are automatically implemented for sketches where the
/// environment implements the draw aspect. These methods are also available
/// on the page for the environment.
pub trait SketchExt<M>
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific + 'static,
{
    /// Get the width of the page.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         assert_eq!(self.width(), self.page.width());
    ///     }
    /// }
    /// ```
    #[inline]
    fn width(&self) -> NumFor<M> {
        self.as_part().size.width
    }

    /// Get the height of the page.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         assert_eq!(self.height(), self.page.height());
    ///     }
    /// }
    /// ```
    #[inline]
    fn height(&self) -> NumFor<M> {
        self.as_part().size.height
    }

    /// Get the size of the page.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         assert_eq!(self.size(), self.page.size());
    ///     }
    /// }
    /// ```
    #[inline]
    fn size(&self) -> Size2<NumFor<M>> {
        self.as_part().size
    }

    /// Set the rectangle mode.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # use sketchbook::aspects::draw::Mode;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.rect_mode(Mode::Corner);
    ///         self.page.rect_mode(Mode::Corner);
    ///
    ///         assert_eq!(self.context().rectangle_mode, Mode::Corner);
    ///     }
    /// }
    /// ```
    #[inline]
    fn rect_mode(&mut self, mode: Mode) {
        self.as_part_mut().context.rectangle_mode = mode;
    }

    /// Get the ellipse mode.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # use sketchbook::aspects::draw::Mode;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.ellipse_mode(Mode::Radius);
    ///         self.page.ellipse_mode(Mode::Radius);
    ///
    ///         assert_eq!(self.context().ellipse_mode, Mode::Corner);
    ///     }
    /// }
    /// ```
    #[inline]
    fn ellipse_mode(&mut self, mode: Mode) {
        self.as_part_mut().context.ellipse_mode = mode;
    }

    /// Set the fill color for shapes.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.fill((100, 100, 100));
    ///         self.page.fill((100, 100, 100));
    ///
    ///         assert_eq!(self.context().fill, (100, 100, 100));
    ///     }
    /// }
    /// ```
    #[inline]
    fn fill<C: Into<ColorFor<M>>>(&mut self, color: C) {
        self.as_part_mut().context.fill = color.into();
    }

    /// Disable fill for shapes.
    ///
    /// This uses the color defined by [`EnvSpecific::no_color()`].
    /// As such, it is equivalent to calling `.fill(no_color())`.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.no_fill();
    ///         self.page.no_fill();
    ///
    ///         assert_eq!(self.context().fill, (0, 0, 0));
    ///     }
    /// }
    /// ```
    #[inline]
    fn no_fill(&mut self) {
        self.as_part_mut().context.fill = M::no_color();
    }

    /// Set the stroke color for shapes.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.stroke((100, 100, 100));
    ///         self.page.stroke((100, 100, 100));
    ///
    ///         assert_eq!(self.context().stroke, (100, 100, 100));
    ///     }
    /// }
    /// ```
    #[inline]
    fn stroke<C: Into<ColorFor<M>>>(&mut self, color: C) {
        self.as_part_mut().context.stroke = color.into();
    }

    /// Disable stroke for shapes.
    ///
    /// This uses the color defined by [`EnvSpecific::no_color()`].
    /// Additionally, the weight of the stroke is set to zero.
    /// As such, it is equivalent to calling
    /// ```ignore
    /// page.stroke(no_color());
    /// page.weight(0);
    /// ```
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.no_stroke();
    ///         self.page.no_stroke();
    ///
    ///         assert_eq!(self.context().stroke, (0, 0, 0));
    ///         assert_eq!(self.context().weight, 0.0);
    ///     }
    /// }
    /// ```
    #[inline]
    fn no_stroke(&mut self) {
        self.as_part_mut().context.stroke = M::no_color();
        self.as_part_mut().context.weight = M::Num::zero();
    }

    /// Set the stroke weight for shapes.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.weight(10.0);
    ///         self.page.weight(10.0);
    ///
    ///         assert_eq!(self.context().weight, 10.0);
    ///     }
    /// }
    /// ```
    #[inline]
    fn weight<R: ToReal<NumFor<M>>>(&mut self, width: R) {
        self.as_part_mut().context.weight = width.to_real();
    }

    /// Set the background color of the page.
    ///
    /// This also clears all previously drawn shapes.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.background((100, 100, 100));
    ///         self.page.background((100, 100, 100));
    ///     }
    /// }
    /// ```
    fn background<C: Into<ColorFor<M>>>(&mut self, color: C) {
        let color = color.into();

        // clear shape buffer
        let part = self.as_part_mut();
        part.shapes.shape_buffer_clear();
        part.shapes
            .shape_buffer_push(Shape::Background(color.clone()));

        // set background color
        part.background = color;
    }

    /// Draw a circle.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.circle((43, 12, 100));
    ///         self.page.circle((43, 12, 100));
    ///     }
    /// }
    /// ```
    fn circle<S: IntoWithContext<Circle<NumFor<M>, ColorFor<M>>, NumFor<M>, ColorFor<M>>>(
        &mut self,
        circle: S,
    ) {
        let circle = circle.into_with_context(&self.as_part().context);
        self.as_part_mut()
            .shapes
            .shape_buffer_push(Shape::Circle(circle));
    }

    /// Draw a rectangle.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.rect((43, 12, 100, 200));
    ///         self.page.rect((43, 12, 100, 200));
    ///     }
    /// }
    /// ```
    fn rect<S: IntoWithContext<Rectangle<NumFor<M>, ColorFor<M>>, NumFor<M>, ColorFor<M>>>(
        &mut self,
        rectangle: S,
    ) {
        let rect = rectangle.into_with_context(&self.as_part().context);
        self.as_part_mut()
            .shapes
            .shape_buffer_push(Shape::Rectangle(rect));
    }

    /// set the frame rate.
    ///
    /// The environment may not be able to provide the requested rate.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         self.frame_rate(60);
    ///         self.page.frame_rate(60);
    ///     }
    /// }
    /// ```
    #[inline]
    fn frame_rate<R: ToReal<NumFor<M>>>(&mut self, rate: R) {
        self.as_part_mut().frame_rate = rate.to_real()
    }

    /// Get the current context.
    ///
    /// ```
    /// # use sketchbook::aspects::draw;
    /// # sketchbook::draw_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(draw))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl draw::Handlers for App {
    ///     fn draw(&mut self) {
    ///         assert_eq!(self.context(), self.page.context());
    ///     }
    /// }
    /// ```
    #[inline]
    fn context(&self) -> &Context<NumFor<M>, ColorFor<M>> {
        &self.as_part().context
    }
}

/// Implemented on sketches and environment pages.
impl<M, T> SketchExt<M> for T
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
    // default impls used
}

/// Run event handler methods based on event.
///
/// A user usually doesn't need to use this directly as the
/// [`crate::derive_sketch`] macro will use it automatically.
/// This trait is implemented for the sketch when the `draw`
/// aspect is passed to the `aspects=(...)` list as seen in the
/// example below.
///
/// ```
/// use sketchbook::aspects::draw;
/// # sketchbook::draw_env!();
/// #[apply(derive_sketch)]
/// #[sketch(env=single_aspect, aspects=(draw))]
/// struct App {
///     #[page]
///     page: single_aspect::Page,
/// }
///
/// impl draw::Handlers for App {
///     fn draw(&mut self) {
///         // This handler will now be called whenever an event happens.
///     }
/// }
/// ```
pub fn handle_event<S, E>(sketch: &mut S, event: &E) -> Result<(), Error>
where
    S: Sketch + Handlers,
    S::Env: AssociatedEnvSpecificMarker + Environment,
    E: TryAsPart<Event<SpecificallyFor<S::Env>>>,
    PageOf<S::Env>: AsPart<EnvData<SpecificallyFor<S::Env>>>,
{
    match event.try_as_part() {
        Some(Event::Draw) => {
            // let context = &mut self.as_part_mut().context;
            // context.scale = 1.;
            // context.rotation = 0.;
            // context.translation = Point { x: 0., y: 0. };
            //
            sketch.draw();
        }
        Some(Event::_MetaPhantom(_, _)) => unreachable!(),
        None => {},
    }
    Ok(())
}

/// Possible events for draw aspect.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Event<M>
where
    M: EnvSpecific,
{
    /// Needed to allow the use of `M`.
    #[doc(hidden)]
    #[cfg_attr(feature = "serde", serde(skip))]
    _MetaPhantom(core::convert::Infallible, PhantomData<M>),

    /// Redraw requested.
    ///
    /// It is common for the environment to emit this event when the page is resized.
    Draw,
}

/// Marker trait for getting environment specific marker type.
pub trait AssociatedEnvSpecificMarker {
    /// Marker type that has environment specific implementations on it.
    type EnvSpecificMarker: EnvSpecific;
}

/// Marker trait for environment specific types.
pub trait EnvSpecific: 'static {
    /// Type for numbers.
    type Num: Real;

    /// Type for colors.
    type Color: Clone;

    /// Type for drawn shape buffer.
    type ShapeBuffer: ShapeBuffer<Self::Num, Self::Color>;

    /// Get the default size of page.
    fn default_size() -> Size2<Self::Num>;

    /// Get the default context for a page.
    fn default_context() -> Context<Self::Num, Self::Color>;

    /// Get the default background color for a page.
    fn default_background() -> Self::Color;

    /// Get the default frame rate for a page.
    fn default_frame_rate() -> Self::Num;

    /// Get value for no color.
    ///
    /// Used by [`SketchExt::no_stroke()`] and [`SketchExt::no_fill()`].
    fn no_color() -> Self::Color;
}

/// Trait for types that can be used as shape buffers.
///
/// This allows environments to do real time drawing instead of having it buffered.
pub trait ShapeBuffer<N, C> {
    /// Push a shape onto the buffer.
    fn shape_buffer_push(&mut self, shape: Shape<N, C>);

    /// Clear the buffer.
    fn shape_buffer_clear(&mut self);
}

#[cfg(feature = "alloc")]
#[allow(unused_qualifications)]
impl<N, C> ShapeBuffer<N, C> for alloc::vec::Vec<Shape<N, C>> {
    #[inline]
    fn shape_buffer_push(&mut self, shape: Shape<N, C>) {
        self.push(shape)
    }

    #[inline]
    fn shape_buffer_clear(&mut self) {
        self.clear()
    }
}

/// Data for draw aspect that the environment's page will hold.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct EnvData<M>
where
    M: EnvSpecific,
{
    /// Store the marker type.
    marker: PhantomData<M>,

    /// Requested frame rate for drawing.
    frame_rate: M::Num,

    /// Shape buffer to push shapes onto.
    shapes: M::ShapeBuffer,

    /// Currently set background color.
    background: M::Color,

    /// Size of the draw area.
    size: Size2<M::Num>,

    /// Current context for creating shapes.
    context: Context<M::Num, M::Color>,
}

impl<M> EnvData<M>
where
    M: EnvSpecific,
{
    /// Set the page's size.
    #[inline]
    pub fn set_size(&mut self, size: Size2<M::Num>) {
        self.size = size;
    }

    /// Get the frame rate.
    #[inline]
    pub fn frame_rate(&self) -> M::Num {
        self.frame_rate
    }

    /// Get the background color.
    #[inline]
    pub fn background_color(&self) -> &M::Color {
        &self.background
    }

    /// Get drawn frame.
    #[inline]
    pub fn shape_buffer_mut(&mut self) -> &mut M::ShapeBuffer {
        &mut self.shapes
    }
}

impl<M> Default for EnvData<M>
where
    M: EnvSpecific,
    M::ShapeBuffer: Default,
{
    #[inline]
    fn default() -> Self {
        Self {
            marker: PhantomData,
            size: M::default_size(),
            context: M::default_context(),
            shapes: M::ShapeBuffer::default(),
            background: M::default_background(),
            frame_rate: M::default_frame_rate(),
        }
    }
}

#[derive(Debug)]
pub enum Error {}

/// Helper type alias to get number type for environment.
pub type NumFor<T> = <T as EnvSpecific>::Num;

/// Helper type alias to get color type for environment.
pub type ColorFor<T> = <T as EnvSpecific>::Color;

/// Helper type alias to get type specifically for environment.
pub type SpecificallyFor<T> = <T as AssociatedEnvSpecificMarker>::EnvSpecificMarker;

#[test]
fn test_draw() {
    use crate::aspects::draw;
    use crate::env::*;
    use crate::*;

    // create environment for test
    mod single_aspect {
        use crate::aspects::draw;
        use crate::aspects::draw::*;

        crate::env_for_aspect!(draw);

        crate::compose! {
            pub enum Events {
                #[part]
                Aspect(Event<EnvSpecificMarker>),
            }
        }

        crate::compose! {
            #[derive(Default)]
            pub struct Page {
                #[part]
                pub aspect: EnvData<EnvSpecificMarker>,
                pub time: i16,
            }
        }

        impl EnvSpecific for EnvSpecificMarker {
            type Num = f32;

            type Color = u8;

            type ShapeBuffer = Vec<Shape<f32, u8>>;

            fn default_size() -> Size2<Self::Num> {
                Size2 {
                    width: 0.0,
                    height: 0.0,
                }
            }

            fn default_context() -> Context<Self::Num, Self::Color> {
                Context {
                    fill: 0,
                    ellipse_mode: Mode::Center,
                    rectangle_mode: Mode::Center,
                    rotation: 0.0,
                    stroke: 0,
                    weight: 0.0,
                }
            }

            fn default_background() -> Self::Color {
                0
            }

            fn default_frame_rate() -> Self::Num {
                0.0
            }

            fn no_color() -> Self::Color {
                0
            }
        }
    }

    // create sketch
    struct App {
        page: single_aspect::Page,
        drew: bool,
    }

    impl Sketch for App {
        type Env = single_aspect::Env;

        fn page(&self) -> &PageOf<Self::Env> {
            &self.page
        }

        fn page_mut(&mut self) -> &mut PageOf<Self::Env> {
            &mut self.page
        }

        fn handle_event(&mut self, event: &EventOf<Self::Env>) -> Result<(), Error> {
            handle_event(self, event)
        }
    }

    impl Setup for App {
        fn setup(page: single_aspect::Page, _: ()) -> Self {
            App { page, drew: false }
        }
    }

    impl Handlers for App {
        fn draw(&mut self) {
            // mark flag when this handler is run
            self.drew = true;
        }
    }

    // create sketch instance.
    let mut mill = single_aspect::Mill;
    let mut app = App::setup(mill.new_page().unwrap(), ());

    assert!(!app.drew);

    // handle draw event
    app.handle_event(&single_aspect::Events::Aspect(draw::Event::Draw)).unwrap();

    // check that handler ran
    assert!(app.drew);
}

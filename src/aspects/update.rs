//! Periodic update.
//!
//! This aspect enables a sketch's state to be update periodically.
//!
//! ```
//! use sketchbook::aspects::update;
//! # sketchbook::update_env!();
//!
//! #[apply(derive_sketch)]
//! #[sketch(env=single_aspect, aspects=(update))]
//! struct App {
//!     #[page]
//!     page: single_aspect::Page,
//! }
//!
//! impl update::Handlers for App {
//!     fn update(&mut self, delta_t: i16) {
//!         println!("Update state, time since last update: {}", delta_t);
//!     }
//! }
//! ```

/// Create test environment for update aspect for use in doc tests.
#[doc(hidden)]
#[macro_export]
macro_rules! update_env {
    {} => {
        use sketchbook::derive_sketch;
        use macro_rules_attribute::apply;
        use sketchbook::aspects::update::SketchExt;
        mod single_aspect {
            use sketchbook::aspects::update;
            use sketchbook::aspects::update::*;

            sketchbook::env_for_aspect!(update);

            sketchbook::compose! {
                pub enum Events {
                    #[part]
                    Aspect(update::Event),
                }
            }

            sketchbook::compose! {
                #[derive(Default)]
                pub struct Page {
                    #[part]
                    pub aspect: update::EnvData<EnvSpecificMarker>,
                }
            }

            impl update::EnvSpecific for EnvSpecificMarker {
                type Time = i16;
                type Duration = i16;
                type Num = f32;

                fn default_update_rate() -> Self::Num {
                    30.0
                }

                fn zero_duration() -> Self::Duration {
                    0
                }

                fn duration_between(start: &Self::Time, end: &Self::Time) -> Self::Duration {
                    end - start
                }
            }

            impl update::EnvPageExt<EnvSpecificMarker> for Page {
                fn get_time(&mut self) -> update::TimeFor<EnvSpecificMarker> {
                    0
                }
            }
        }
    }
}

use core::marker::PhantomData;

use crate::{compose::{AsPart, TryAsPart}, Environment, PageOf, Sketch};

/// Event handlers for update aspect.
///
/// Each handler has a default implementation that does nothing.
/// Implement a method to perform actions when the associated event happens.
pub trait Handlers
where
    Self: Sketch,
    Self::Env: AssociatedEnvSpecificMarker,
{
    /// Handler for [`Event::Update`] event.
    ///
    /// Parameter `delta_t` is the time since the last call to update.
    #[allow(unused_variables)]
    #[inline]
    fn update(&mut self, delta_t: DurationFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }
}

/// Extension methods for sketch.
///
/// These methods are automatically implemented for sketches where the
/// environment implements the draw aspect. These methods are also available
/// on the page for the environment.
pub trait SketchExt<M>
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
    /// Stop running this sketch.
    ///
    /// This will also stop all other aspects and result in dropping the sketch.
    ///
    /// ```
    /// # use sketchbook::aspects::update;
    /// # sketchbook::update_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(update))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl update::Handlers for App {
    ///     fn update(&mut self, delta_t: i16) {
    ///         self.stop();
    ///         self.page.stop();
    ///     }
    /// }
    /// ```
    #[inline]
    fn stop(&mut self) {
        self.as_part_mut().should_stop = true;
    }

    /// Pause updates for this sketch.
    ///
    /// ```
    /// # use sketchbook::aspects::update;
    /// # sketchbook::update_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(update))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl update::Handlers for App {
    ///     fn update(&mut self, delta_t: i16) {
    ///         self.pause();
    ///         self.page.pause();
    ///     }
    /// }
    /// ```
    #[inline]
    fn pause(&mut self) {
        self.as_part_mut().is_paused = true;
    }

    /// Resume updates for this sketch.
    ///
    /// ```
    /// # use sketchbook::aspects::update;
    /// # sketchbook::update_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(update))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl update::Handlers for App {
    ///     fn update(&mut self, delta_t: i16) {
    ///         self.resume();
    ///         self.page.resume();
    ///     }
    /// }
    /// ```
    #[inline]
    fn resume(&mut self) {
        self.as_part_mut().is_paused = false;
    }

    /// Check if sketch is paused.
    ///
    /// ```
    /// # use sketchbook::aspects::update;
    /// # sketchbook::update_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(update))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl update::Handlers for App {
    ///     fn update(&mut self, delta_t: i16) {
    ///         assert_eq!(self.is_paused(), self.page.is_paused());
    ///     }
    /// }
    /// ```
    #[inline]
    fn is_paused(&self) -> bool {
        self.as_part().is_paused
    }

    /// Set the rate the update handler is called.
    ///
    /// The environment may not be able to actually call the update handler at the rate given.
    ///
    /// ```
    /// # use sketchbook::aspects::update;
    /// # sketchbook::update_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(update))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl update::Handlers for App {
    ///     fn update(&mut self, delta_t: i16) {
    ///         self.update_rate(60.0);
    ///         self.page.update_rate(60.0);
    ///     }
    /// }
    /// ```
    #[inline]
    fn update_rate<R: crate::real::ToReal<M::Num>>(&mut self, rate: R) {
        self.as_part_mut().update_rate = rate.to_real();
    }
}

/// Implemented on sketches and environment pages.
impl<M, T> SketchExt<M> for T
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
    // default impls used
}

/// Run event handler methods based on event.
///
/// A user usually doesn't need to use this directly as the
/// [`crate::derive_sketch`] macro will use it automatically.
/// This trait is implemented for the sketch when the `update`
/// aspect is passed to the `aspects=(...)` list as seen in the
/// example below.
///
/// ```
/// use sketchbook::aspects::update;
/// # sketchbook::update_env!();
/// #[apply(derive_sketch)]
/// #[sketch(env=single_aspect, aspects=(update))]
/// struct App {
///     #[page]
///     page: single_aspect::Page,
/// }
///
/// impl update::Handlers for App {
///     fn update(&mut self, delta_t: i16) {
///         // This handler will now be called whenever an event happens.
///     }
/// }
/// ```
pub fn handle_event<S, E>(sketch: &mut S, event: &E) -> Result<(), Error>
where
    S: Sketch + Handlers,
    S::Env: AssociatedEnvSpecificMarker + Environment,
    E: TryAsPart<Event>,
    PageOf<S::Env>:
        EnvPageExt<SpecificallyFor<S::Env>> + AsPart<EnvData<SpecificallyFor<S::Env>>>
{

    match event.try_as_part() {
        Some(Event::Update) => {
            let page = sketch.page_mut();
            let now = page.get_time();
            let delta_t = if let Some(last_time) = &page.as_part().last_time {
                SpecificallyFor::<S::Env>::duration_between(last_time, &now)
            } else {
                SpecificallyFor::<S::Env>::zero_duration()
            };
            sketch.update(delta_t);
            sketch.page_mut().as_part_mut().last_time = Some(now);
        }
        None => {},
    }
    Ok(())
}

/// Possible events for update aspect.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Event {
    /// Update sketch state.
    Update,
}

/// Marker trait for getting environment specific marker type.
pub trait AssociatedEnvSpecificMarker {
    /// Marker type that has environment specific implementations on it.
    type EnvSpecificMarker: EnvSpecific;
}

/// Marker trait for environment specific types.
pub trait EnvSpecific {
    /// Type for times.
    type Time;

    /// Type for duration between times.
    type Duration;

    /// Type for numbers.
    type Num;

    /// Default value for update rate.
    fn default_update_rate() -> Self::Num;

    /// Value for zero duration.
    fn zero_duration() -> Self::Duration;

    /// Duration between two times.
    fn duration_between(start: &Self::Time, end: &Self::Time) -> Self::Duration;
}

/// Data for update aspect that the environment's page will hold.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(
    feature = "serde",
    serde(
        bound = "M::Num: serde::Serialize + serde::de::DeserializeOwned, M::Time: serde::Serialize + serde::de::DeserializeOwned"
    )
)]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct EnvData<M>
where
    M: EnvSpecific,
{
    marker: PhantomData<M>,
    update_rate: M::Num,
    is_paused: bool,
    should_stop: bool,
    last_time: Option<M::Time>,
}

impl<M> EnvData<M>
where
    M: EnvSpecific,
{
    /// Get should stop flag.
    #[inline]
    pub fn should_stop(&self) -> bool {
        self.should_stop
    }

    /// Get the update rate.
    #[inline]
    pub fn update_rate(&self) -> &M::Num {
        &self.update_rate
    }

    /// Single to stop executing the sketch.
    #[inline]
    pub fn stop(&mut self) {
        self.should_stop = true;
    }
}

impl<M> Default for EnvData<M>
where
    M: EnvSpecific,
{
    #[inline]
    fn default() -> Self {
        Self {
            marker: PhantomData,
            update_rate: M::default_update_rate(),
            is_paused: false,
            should_stop: false,
            last_time: None,
        }
    }
}

/// Extra behavior needed on environment.
pub trait EnvPageExt<M>
where
    M: EnvSpecific,
{
    /// Get the current time.
    fn get_time(&mut self) -> M::Time;
}

#[derive(Debug)]
pub enum Error {}

/// Helper type alias to get time type for environment.
pub type TimeFor<T> = <T as EnvSpecific>::Time;

/// Helper type alias to get duration type for environment.
pub type DurationFor<T> = <T as EnvSpecific>::Duration;

/// Helper type alias to get num type for environment.
pub type NumFor<T> = <T as EnvSpecific>::Num;

/// Helper type alias to get type specifically for environment.
pub type SpecificallyFor<T> = <T as AssociatedEnvSpecificMarker>::EnvSpecificMarker;

/// Implementation of update aspect using Instant.
#[cfg(feature = "std")]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct UpdateUsingInstant {
    last_update: Option<std::time::Instant>,
}

#[cfg(feature = "std")]
impl UpdateUsingInstant {
    /// Check if an update should happen. If an update should happen, then the time of calling
    /// this function is recorded to compare against the next call time.
    pub fn should_update(&mut self, update_rate: f32) -> bool {
        let now = std::time::Instant::now();
        if let Some(last_update) = self.last_update {
            if now.duration_since(last_update).as_secs_f32() >= 1. / update_rate {
                // time has expired so flag for update
                self.last_update = Some(now);
                true
            } else {
                false
            }
        } else {
            // this is the first check so flag for update
            self.last_update = Some(now);
            true
        }
    }
}

#[test]
fn test_update() {
    use crate::aspects::update;
    use crate::env::*;
    use crate::*;

    mod single_aspect {
        use crate::aspects::update;

        crate::env_for_aspect!(update);

        crate::compose! {
            pub enum Events {
                #[part]
                Aspect(super::Event),
            }
        }

        crate::compose! {
            #[derive(Default)]
            pub struct Page {
                #[part]
                pub aspect: super::EnvData<EnvSpecificMarker>,
                pub time: i16,
            }
        }

        impl update::EnvSpecific for EnvSpecificMarker {
            type Time = i16;
            type Duration = i16;
            type Num = f32;

            fn default_update_rate() -> Self::Num {
                30.0
            }

            fn zero_duration() -> Self::Duration {
                0
            }

            fn duration_between(start: &Self::Time, end: &Self::Time) -> Self::Duration {
                end - start
            }
        }

        impl update::EnvPageExt<EnvSpecificMarker> for Page {
            fn get_time(&mut self) -> update::TimeFor<EnvSpecificMarker> {
                self.time
            }
        }
    }

    struct App {
        page: single_aspect::Page,
        updated: bool,
    }

    impl Sketch for App {
        type Env = single_aspect::Env;

        fn page(&self) -> &single_aspect::Page {
            &self.page
        }

        fn page_mut(&mut self) -> &mut single_aspect::Page {
            &mut self.page
        }

        fn handle_event(&mut self, event: &EventOf<Self::Env>) -> Result<(), Error> {
            handle_event(self, event)
        }
    }

    impl Setup for App {
        fn setup(page: single_aspect::Page, _: ()) -> Self {
            App {
                page,
                updated: false,
            }
        }
    }

    impl Handlers for App {
        fn update(&mut self, delta_t: i16) {
            if self.updated {
                assert_eq!(delta_t, 123);
            } else {
                assert_eq!(delta_t, 0);
                self.updated = true;
            }
        }
    }

    let mut mill = single_aspect::Mill;

    let mut app = App::setup(mill.new_page().unwrap(), ());

    // send update event
    app.handle_event(&single_aspect::Events::Aspect(update::Event::Update)).unwrap();

    assert!(app.updated);

    // change time and send update event
    app.page.time = 123;
    app.handle_event(&single_aspect::Events::Aspect(update::Event::Update)).unwrap();

    assert_eq!(app.page.aspect.update_rate, 30.0);
    assert!(!app.page.aspect.is_paused);
    assert!(!app.page.aspect.should_stop);
    assert_eq!(app.page.aspect.last_time, Some(123));

    app.stop();
    assert!(app.page.aspect.should_stop);

    app.pause();
    assert!(app.page.aspect.is_paused);
    assert!(app.is_paused());

    app.resume();
    assert!(!app.page.aspect.is_paused);
    assert!(!app.is_paused());

    app.update_rate(10.0_f64);
    assert_eq!(app.page.aspect.update_rate, 10.0);
}

#[cfg(feature = "std")]
#[test]
fn test_update_instant_impl() {
    let mut update_impl = UpdateUsingInstant::default();

    let rate = 1_000_000.0;

    assert!(update_impl.should_update(rate));
    assert!(!update_impl.should_update(rate));
    std::thread::sleep(std::time::Duration::from_nanos(10));
    assert!(update_impl.should_update(rate));
    assert!(!update_impl.should_update(rate));
    std::thread::sleep(std::time::Duration::from_nanos(10));
    assert!(update_impl.should_update(rate));
    assert!(!update_impl.should_update(rate));
}

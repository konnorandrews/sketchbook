//! Mouse input.
//!
//! This aspect enables mouse input for an environment.
//!
//! ```
//! use sketchbook::aspects::mouse;
//! # sketchbook::mouse_env!();
//!
//! #[apply(derive_sketch)]
//! #[sketch(env=single_aspect, aspects=(mouse))]
//! struct App {
//!     #[page]
//!     page: single_aspect::Page,
//! }
//!
//! impl mouse::Handlers for App {
//!     fn moved(&mut self, previous: Point2<f32>) {
//!         println!("Mouse moved");
//!     }
//! }
//! ```

/// Create test environment for keyboard aspect for use in doc tests.
#[doc(hidden)]
#[macro_export]
macro_rules! mouse_env {
    {} => {
        use sketchbook::derive_sketch;
        use sketchbook::Point2;
        use macro_rules_attribute::apply;
        use sketchbook::aspects::mouse::SketchExt;
        mod single_aspect {
            use sketchbook::aspects::mouse;
            use sketchbook::aspects::mouse::*;

            sketchbook::env_for_aspect!(mouse);

            sketchbook::compose! {
                pub enum Events {
                    #[part]
                    Aspect(mouse::Event<EnvSpecificMarker>),
                }
            }

            sketchbook::compose! {
                #[derive(Default)]
                pub struct Page {
                    #[part]
                    pub aspect: mouse::EnvData<EnvSpecificMarker>,
                }
            }

            impl mouse::EnvSpecific for EnvSpecificMarker {
                type Button = u8;
                type ButtonSet = std::collections::HashSet<u8>;
                type Num = f32;
            }
        }
    }
}

use core::{borrow::Borrow, marker::PhantomData, ops::Add};

use crate::{compose::{AsPart, TryAsPart}, Environment, PageOf, Point2, Sketch};

/// Event handlers for mouse aspect.
///
/// Each handler has a default implementation that does nothing.
/// Implement a method to perform actions when the associated event happens.
pub trait Handlers
where
    Self: Sketch,
    Self::Env: AssociatedEnvSpecificMarker,
{
    /// Mouse moved while any button is held.
    #[allow(unused_variables)]
    #[inline]
    fn dragged(&mut self, previous: Point2<NumFor<SpecificallyFor<Self::Env>>>) {
        // by default do nothing
    }

    /// Mouse moved with no buttons pressed.
    #[allow(unused_variables)]
    #[inline]
    fn moved(&mut self, previous: Point2<NumFor<SpecificallyFor<Self::Env>>>) {
        // by default do nothing
    }

    /// Mouse button was clicked (press followed by release).
    #[allow(unused_variables)]
    #[inline]
    fn clicked(&mut self, button: &ButtonFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }

    /// Handler for [`Event::Press`] event.
    #[allow(unused_variables)]
    #[inline]
    fn pressed(&mut self, button: &ButtonFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }

    /// Handler for [`Event::Release`] event.
    #[allow(unused_variables)]
    #[inline]
    fn released(&mut self, button: &ButtonFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }

    /// Handler for [`Event::Wheel`] event.
    #[allow(unused_variables)]
    #[inline]
    fn wheel(&mut self, count: &NumFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }
}

/// Extension methods for sketch.
///
/// These methods are automatically implemented for sketches where the
/// environment implements the draw aspect. These methods are also available
/// on the page for the environment.
pub trait SketchExt<M>
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific + 'static,
{
    /// Get the current x position of the mouse.
    ///
    /// ```
    /// # use sketchbook::aspects::mouse;
    /// # sketchbook::mouse_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(mouse))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl mouse::Handlers for App {
    ///     fn moved(&mut self, previous: Point2<f32>) {
    ///         assert_eq!(self.mouse_x(), self.page.mouse_x());
    ///     }
    /// }
    /// ```
    #[inline]
    fn mouse_x(&self) -> &NumFor<M> {
        &self.as_part().position.x
    }

    /// Get the current y position of the mouse.
    ///
    /// ```
    /// # use sketchbook::aspects::mouse;
    /// # sketchbook::mouse_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(mouse))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl mouse::Handlers for App {
    ///     fn moved(&mut self, previous: Point2<f32>) {
    ///         assert_eq!(self.mouse_y(), self.page.mouse_y());
    ///     }
    /// }
    /// ```
    #[inline]
    fn mouse_y(&self) -> &NumFor<M> {
        &self.as_part().position.y
    }

    /// Get the current position of the mouse.
    ///
    /// ```
    /// # use sketchbook::aspects::mouse;
    /// # sketchbook::mouse_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(mouse))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl mouse::Handlers for App {
    ///     fn moved(&mut self, previous: Point2<f32>) {
    ///         assert_eq!(self.mouse_point(), self.page.mouse_point());
    ///     }
    /// }
    /// ```
    #[inline]
    fn mouse_point(&self) -> &Point2<NumFor<M>> {
        &self.as_part().position
    }

    /// Check if a mouse button is pressed.
    ///
    /// ```
    /// # use sketchbook::aspects::mouse;
    /// # sketchbook::mouse_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(mouse))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl mouse::Handlers for App {
    ///     fn moved(&mut self, previous: Point2<f32>) {
    ///         assert_eq!(self.button_pressed(2), self.page.button_pressed(2));
    ///     }
    /// }
    /// ```
    #[inline]
    fn button_pressed<B: Borrow<ButtonFor<M>>>(&self, button: B) -> bool {
        self.as_part()
            .pressed_buttons
            .button_set_contains(button.borrow())
    }

    /// Check if any mouse buttons are pressed.
    ///
    /// ```
    /// # use sketchbook::aspects::mouse;
    /// # sketchbook::mouse_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(mouse))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl mouse::Handlers for App {
    ///     fn moved(&mut self, previous: Point2<f32>) {
    ///         assert_eq!(self.any_button_pressed(), self.page.any_button_pressed());
    ///     }
    /// }
    /// ```
    #[inline]
    fn any_button_pressed(&self) -> bool {
        !self.as_part().pressed_buttons.button_set_is_empty()
    }
}

impl<M, T> SketchExt<M> for T
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
}

/// Run event handler methods based on event.
///
/// A user usually doesn't need to use this directly as the
/// [`crate::derive_sketch`] macro will use it automatically.
/// This trait is implemented for the sketch when the `mouse`
/// aspect is passed to the `aspects=(...)` list as seen in the
/// example below.
///
/// ```
/// use sketchbook::aspects::mouse;
/// # sketchbook::mouse_env!();
/// #[apply(derive_sketch)]
/// #[sketch(env=single_aspect, aspects=(mouse))]
/// struct App {
///     #[page]
///     page: single_aspect::Page,
/// }
///
/// impl mouse::Handlers for App {
///     fn moved(&mut self, previous: Point2<f32>) {
///         // This handler will now be called whenever an event happens.
///     }
/// }
/// ```
pub fn handle_event<S, E>(sketch: &mut S, event: &E) -> Result<(), Error>
where
    S: Sketch + Handlers,
    S::Env: AssociatedEnvSpecificMarker + Environment,
    E: TryAsPart<Event<SpecificallyFor<S::Env>>>,
    PageOf<S::Env>: AsPart<EnvData<SpecificallyFor<S::Env>>>,
{
    match event.try_as_part() {
        Some(Event::MoveRelative(point)) => {
            let data = sketch.page_mut().as_part_mut();
            let new_position = &data.position + point;
            let previous = core::mem::replace(&mut data.position, new_position);
            if data.pressed_buttons.button_set_is_empty() {
                sketch.moved(previous);
            } else {
                sketch.dragged(previous);
            }
        }
        Some(Event::MoveAbsolute(point)) => {
            let data = sketch.page_mut().as_part_mut();
            let previous = core::mem::replace(&mut data.position, point.clone());
            if data.pressed_buttons.button_set_is_empty() {
                sketch.moved(previous);
            } else {
                sketch.dragged(previous);
            }
        }
        Some(Event::Wheel(count)) => {
            sketch.wheel(count);
        }
        Some(Event::Press(button)) => {
            let data = sketch.page_mut().as_part_mut();
            data.pressed_buttons.button_set_insert(button.clone());
            sketch.pressed(button);
        }
        Some(Event::Release(button)) => {
            let data = sketch.page_mut().as_part_mut();
            let was_pressed = data.pressed_buttons.button_set_remove(button);
            sketch.released(button);
            if was_pressed {
                sketch.clicked(button);
            }
        }
        Some(Event::_MetaPhantom(_, _)) => unreachable!(),
        None => {}
    }
    Ok(())
}

/// Possible events for keyboard aspect.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Event<M>
where
    M: EnvSpecific,
{
    /// Needed to allow the use of `M`.
    #[doc(hidden)]
    #[cfg_attr(feature = "serde", serde(skip))]
    _MetaPhantom(core::convert::Infallible, PhantomData<M>),

    /// Mouse move relative position.
    MoveRelative(Point2<M::Num>),

    /// Mouse move absolute position.
    MoveAbsolute(Point2<M::Num>),

    /// Mouse button pressed.
    Press(M::Button),

    /// Mouse button released.
    Release(M::Button),

    /// Scroll wheel moved.
    Wheel(M::Num),
}

/// Marker trait for getting environment specific marker type.
pub trait AssociatedEnvSpecificMarker {
    /// Marker type that has environment specific implementations on it.
    type EnvSpecificMarker: EnvSpecific;
}

/// Marker trait for environment specific types.
pub trait EnvSpecific: 'static {
    /// Type used for mouse buttons.
    type Button: Clone;

    /// Type used for numbers.
    type Num: Default + Add<Output = Self::Num> + Clone;

    /// Type for set of buttons pressed.
    type ButtonSet: ButtonSet<Self::Button>;
}

/// Trait for types that can be used as a pressed button set.
pub trait ButtonSet<B> {
    /// Check if the set is empty.
    fn button_set_is_empty(&self) -> bool;

    /// Insert key into set.
    fn button_set_insert(&mut self, value: B);

    /// Check if set contains key.
    fn button_set_contains(&self, value: &B) -> bool;

    /// Remove key from set.
    fn button_set_remove(&mut self, value: &B) -> bool;
}

#[cfg(feature = "std")]
impl<B> ButtonSet<B> for std::collections::HashSet<B>
where
    B: std::hash::Hash + Eq,
{
    #[inline]
    fn button_set_is_empty(&self) -> bool {
        self.is_empty()
    }

    #[inline]
    fn button_set_insert(&mut self, value: B) {
        let _ = self.insert(value);
    }

    #[inline]
    fn button_set_remove(&mut self, value: &B) -> bool {
        self.remove(value)
    }

    #[inline]
    fn button_set_contains(&self, value: &B) -> bool {
        self.contains(value)
    }
}

#[cfg(feature = "alloc")]
impl<B> ButtonSet<B> for alloc::collections::BTreeSet<B>
where
    B: Ord,
{
    #[inline]
    fn button_set_is_empty(&self) -> bool {
        self.is_empty()
    }

    #[inline]
    fn button_set_insert(&mut self, value: B) {
        let _ = self.insert(value);
    }

    #[inline]
    fn button_set_remove(&mut self, value: &B) -> bool {
        self.remove(value)
    }

    #[inline]
    fn button_set_contains(&self, value: &B) -> bool {
        self.contains(value)
    }
}

/// Data for mouse aspect that the environment's page will hold.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(
    feature = "serde",
    serde(
        bound = "M::Num: serde::Serialize + serde::de::DeserializeOwned, M::ButtonSet: serde::Serialize + serde::de::DeserializeOwned"
    )
)]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct EnvData<M>
where
    M: EnvSpecific,
{
    /// Store the marker type.
    marker: PhantomData<M>,

    /// Current position of the mouse.
    position: Point2<M::Num>,

    /// Buttons currently pressed.
    pressed_buttons: M::ButtonSet,
}

impl<M> Default for EnvData<M>
where
    M: EnvSpecific,
    M::ButtonSet: Default,
{
    #[inline]
    fn default() -> Self {
        Self {
            marker: PhantomData,
            pressed_buttons: M::ButtonSet::default(),
            position: Point2 {
                x: M::Num::default(),
                y: M::Num::default(),
            },
        }
    }
}

#[derive(Debug)]
pub enum Error {}

/// Helper type alias to get button type for environment.
pub type ButtonFor<T> = <T as EnvSpecific>::Button;

/// Helper type alias to get number type for environment.
pub type NumFor<T> = <T as EnvSpecific>::Num;

/// Helper type alias to get type specifically for environment.
pub type SpecificallyFor<T> = <T as AssociatedEnvSpecificMarker>::EnvSpecificMarker;

#[test]
fn test_mouse() {
    use crate::aspects::mouse;
    use crate::env::*;
    use crate::*;

    mod single_aspect {
        use std::collections::HashSet;

        use crate::aspects::mouse;

        crate::env_for_aspect!(mouse);

        crate::compose! {
            pub enum Events {
                #[part]
                Aspect(super::Event<EnvSpecificMarker>),
            }
        }

        crate::compose! {
            #[derive(Default)]
            pub struct Page {
                #[part]
                pub aspect: super::EnvData<EnvSpecificMarker>,
            }
        }

        impl mouse::EnvSpecific for EnvSpecificMarker {
            type Button = u8;
            type ButtonSet = HashSet<u8>;
            type Num = f32;
        }
    }

    struct App {
        page: single_aspect::Page,
        dragged: bool,
        moved: bool,
        clicked: bool,
        pressed: bool,
        released: bool,
        wheel: bool,
    }

    impl Sketch for App {
        type Env = single_aspect::Env;

        fn page(&self) -> &single_aspect::Page {
            &self.page
        }

        fn page_mut(&mut self) -> &mut single_aspect::Page {
            &mut self.page
        }

        fn handle_event(&mut self, event: &EventOf<Self::Env>) -> Result<(), Error> {
            handle_event(self, event)
        }
    }

    impl Setup for App {
        fn setup(page: single_aspect::Page, _: ()) -> Self {
            App {
                page,
                dragged: false,
                moved: false,
                clicked: false,
                pressed: false,
                released: false,
                wheel: false,
            }
        }
    }

    impl Handlers for App {
        fn dragged(&mut self, previous: Point2<f32>) {
            assert_eq!(previous, Point2 { x: 1.0, y: -2.0 });
            assert_eq!(*self.mouse_x(), -2.0);
            assert_eq!(*self.mouse_y(), 1.0);
            assert_eq!(*self.mouse_point(), Point2 { x: -2.0, y: 1.0 });
            self.dragged = true;
        }

        fn moved(&mut self, previous: Point2<f32>) {
            assert_eq!(previous, Point2 { x: 0.0, y: 0.0 });
            assert_eq!(*self.mouse_x(), 1.0);
            assert_eq!(*self.mouse_y(), -2.0);
            assert_eq!(*self.mouse_point(), Point2 { x: 1.0, y: -2.0 });
            self.moved = true;
        }

        fn clicked(&mut self, &button: &u8) {
            assert_eq!(button, 4);
            self.clicked = true;
        }

        fn pressed(&mut self, &button: &u8) {
            assert_eq!(button, 4);
            self.pressed = true;
        }

        fn released(&mut self, &button: &u8) {
            assert_eq!(button, 4);
            self.released = true;
        }

        fn wheel(&mut self, count: &f32) {
            assert_eq!(*count, -20.0);
            self.wheel = true;
        }
    }

    let mut mill = single_aspect::Mill;

    let mut app = App::setup(mill.new_page().unwrap(), ());

    // move mouse
    app.handle_event(&single_aspect::Events::Aspect(mouse::Event::MoveAbsolute(
        Point2 { x: 1.0, y: -2.0 },
    ))).unwrap();

    // press button
    app.handle_event(&single_aspect::Events::Aspect(mouse::Event::Press(4))).unwrap();

    // move mouse (this becomes a drag event)
    app.handle_event(&single_aspect::Events::Aspect(mouse::Event::MoveRelative(
        Point2 { x: -3.0, y: 3.0 },
    ))).unwrap();

    // release button
    app.handle_event(&single_aspect::Events::Aspect(mouse::Event::Release(4))).unwrap();

    // move wheel
    app.handle_event(&single_aspect::Events::Aspect(mouse::Event::Wheel(-20.0))).unwrap();

    assert!(app.moved);
    assert!(app.pressed);
    assert!(app.dragged);
    assert!(app.released);
    assert!(app.clicked);
    assert!(app.wheel);
}

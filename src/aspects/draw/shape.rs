use super::circle::Circle;
use super::rectangle::Rectangle;

/// Shapes that can be drawn.
///
/// ```
/// # use sketchbook::aspects::draw::Shape;
/// #
/// // Example of creating a shape.
/// let shape: Shape<f32, (u8, u8, u8)> = Shape::Background((1, 2, 3));
/// ```
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Shape<Num, Color> {
    /// A circle.
    Circle(Circle<Num, Color>),

    /// A rectangle.
    Rectangle(Rectangle<Num, Color>),

    /// Set background.
    Background(Color),
}

impl<Num, Color> From<Circle<Num, Color>> for Shape<Num, Color> {
    #[inline]
    fn from(circle: Circle<Num, Color>) -> Shape<Num, Color> {
        Shape::Circle(circle)
    }
}

impl<Num, Color> From<Rectangle<Num, Color>> for Shape<Num, Color> {
    #[inline]
    fn from(rectangle: Rectangle<Num, Color>) -> Shape<Num, Color> {
        Shape::Rectangle(rectangle)
    }
}

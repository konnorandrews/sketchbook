//! Keyboard input.
//!
//! This aspect enables keyboard input for an environment.
//!
//! ```
//! use sketchbook::aspects::keyboard;
//! # sketchbook::keyboard_env!();
//!
//! #[apply(derive_sketch)]
//! #[sketch(env=single_aspect, aspects=(keyboard))]
//! struct App {
//!     #[page]
//!     page: single_aspect::Page,
//! }
//!
//! impl keyboard::Handlers for App {
//!     fn pressed(&mut self, key: &u8) {
//!         println!("Key pressed: {}", key);
//!     }
//! }
//! ```

/// Create test environment for keyboard aspect for use in doc tests.
#[doc(hidden)]
#[macro_export]
macro_rules! keyboard_env {
    {} => {
        use sketchbook::derive_sketch;
        use macro_rules_attribute::apply;
        use sketchbook::aspects::keyboard::SketchExt;
        mod single_aspect {
            use sketchbook::aspects::keyboard;
            use sketchbook::aspects::keyboard::*;

            sketchbook::env_for_aspect!(keyboard);

            sketchbook::compose! {
                pub enum Events {
                    #[part]
                    Aspect(keyboard::Event<EnvSpecificMarker>),
                }
            }

            sketchbook::compose! {
                #[derive(Default)]
                pub struct Page {
                    #[part]
                    pub aspect: keyboard::EnvData<EnvSpecificMarker>,
                }
            }

            impl keyboard::EnvSpecific for EnvSpecificMarker {
                type Key = u8;

                type KeySet = std::collections::HashSet<u8>;
            }
        }
    }
}

use core::{borrow::Borrow, marker::PhantomData};

use crate::{compose::{AsPart, TryAsPart}, Environment, PageOf, Sketch};

/// Event handlers for keyboard aspect.
///
/// Each handler has a default implementation that does nothing.
/// Implement a method to perform actions when the associated event happens.
pub trait Handlers
where
    Self: Sketch,
    Self::Env: AssociatedEnvSpecificMarker,
{
    /// Handler for [`Event::Press`] event.
    #[allow(unused_variables)]
    #[inline]
    fn pressed(&mut self, key: &KeyFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }

    /// Handler for [`Event::Repeat`] event.
    #[allow(unused_variables)]
    #[inline]
    fn repeated(&mut self, key: &KeyFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }

    /// Handler for [`Event::Release`] event.
    #[allow(unused_variables)]
    #[inline]
    fn released(&mut self, key: &KeyFor<SpecificallyFor<Self::Env>>) {
        // by default do nothing
    }
}

/// Extension methods for sketch.
///
/// These methods are automatically implemented for sketches where the
/// environment implements the draw aspect. These methods are also available
/// on the page for the environment.
pub trait SketchExt<M>
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
    /// Check if key is currently pressed.
    ///
    /// ```
    /// # use sketchbook::aspects::keyboard;
    /// # sketchbook::keyboard_env!();
    /// # #[apply(derive_sketch)]
    /// # #[sketch(env=single_aspect, aspects=(keyboard))]
    /// # struct App {
    /// #     #[page]
    /// #     page: single_aspect::Page,
    /// # }
    /// #
    /// impl keyboard::Handlers for App {
    ///     fn released(&mut self, key: &u8) {
    ///         assert_eq!(self.key_pressed(2), self.page.key_pressed(2));
    ///     }
    /// }
    /// ```
    #[inline]
    fn key_pressed<K: Borrow<KeyFor<M>>>(&self, key: K) -> bool {
        self.as_part().key_pressed(key.borrow())
    }
}

/// Implemented on sketches and environment pages.
impl<M, T> SketchExt<M> for T
where
    Self: AsPart<EnvData<M>>,
    M: EnvSpecific,
{
    // default impls used
}

/// Run event handler methods based on event.
///
/// A user usually doesn't need to use this directly as the
/// [`crate::derive_sketch`] macro will use it automatically.
/// This trait is implemented for the sketch when the `keyboard`
/// aspect is passed to the `aspects=(...)` list as seen in the
/// example below.
///
/// ```
/// use sketchbook::aspects::keyboard;
/// # sketchbook::keyboard_env!();
/// #[apply(derive_sketch)]
/// #[sketch(env=single_aspect, aspects=(keyboard))]
/// struct App {
///     #[page]
///     page: single_aspect::Page,
/// }
///
/// impl keyboard::Handlers for App {
///     fn pressed(&mut self, key: &u8) {
///         // This handler will now be called whenever an event happens.
///     }
/// }
/// ```
pub fn handle_event<S, E>(sketch: &mut S, event: &E) -> Result<(), Error>
where
    S: Sketch + Handlers,
    S::Env: AssociatedEnvSpecificMarker + Environment,
    E: TryAsPart<Event<SpecificallyFor<S::Env>>>,
    PageOf<S::Env>: AsPart<EnvData<SpecificallyFor<S::Env>>>,
{
    match event.try_as_part() {
        Some(Event::Press(key)) => {
            let data = sketch.page_mut().as_part_mut();
            data.pressed_keys.key_set_insert(key.clone());
            sketch.pressed(key);
        }
        Some(Event::Repeat(key)) => {
            let data = sketch.page_mut().as_part_mut();
            data.pressed_keys.key_set_insert(key.clone());
            sketch.repeated(key);
        }
        Some(Event::Release(key)) => {
            let data = sketch.page_mut().as_part_mut();
            let _was_pressed = data.pressed_keys.key_set_remove(key);
            sketch.released(key);
        }
        Some(Event::_MetaPhantom(_, _)) => unreachable!(),
        None => {}
    }
    Ok(())
}

/// Possible events for keyboard aspect.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Event<M>
where
    M: EnvSpecific,
{
    /// Needed to allow the use of `M`.
    #[doc(hidden)]
    #[cfg_attr(feature = "serde", serde(skip))]
    _MetaPhantom(core::convert::Infallible, PhantomData<M>),

    /// Key is pressed.
    Press(M::Key),

    /// Key is repeated.
    ///
    /// This happens in some environments when a key is held.
    Repeat(M::Key),

    /// Key is released.
    Release(M::Key),
}

/// Marker trait for getting environment specific marker type.
pub trait AssociatedEnvSpecificMarker {
    /// Marker type that has environment specific implementations on it.
    type EnvSpecificMarker: EnvSpecific;
}

/// Marker trait for environment specific types.
pub trait EnvSpecific: 'static {
    /// Type used for keyboard keys.
    type Key: Ord + Clone;

    /// Type for set of keys pressed.
    type KeySet: KeySet<Self::Key>;
}

/// Trait for types that can be used as a pressed key set.
pub trait KeySet<B> {
    /// Check if the set is empty.
    fn key_set_is_empty(&self) -> bool;

    /// Insert key into set.
    fn key_set_insert(&mut self, value: B);

    /// Check if set contains key.
    fn key_set_contains(&self, value: &B) -> bool;

    /// Remove key from set.
    fn key_set_remove(&mut self, value: &B) -> bool;
}

#[cfg(feature = "std")]
impl<B> KeySet<B> for std::collections::HashSet<B>
where
    B: std::hash::Hash + Eq,
{
    #[inline]
    fn key_set_is_empty(&self) -> bool {
        self.is_empty()
    }

    #[inline]
    fn key_set_insert(&mut self, value: B) {
        let _ = self.insert(value);
    }

    #[inline]
    fn key_set_remove(&mut self, value: &B) -> bool {
        self.remove(value)
    }

    #[inline]
    fn key_set_contains(&self, value: &B) -> bool {
        self.contains(value)
    }
}

#[cfg(feature = "alloc")]
impl<B> KeySet<B> for alloc::collections::BTreeSet<B>
where
    B: Ord,
{
    #[inline]
    fn key_set_is_empty(&self) -> bool {
        self.is_empty()
    }

    #[inline]
    fn key_set_insert(&mut self, value: B) {
        let _ = self.insert(value);
    }

    #[inline]
    fn key_set_remove(&mut self, value: &B) -> bool {
        self.remove(value)
    }

    #[inline]
    fn key_set_contains(&self, value: &B) -> bool {
        self.contains(value)
    }
}

/// Data for keyboard aspect that the environment's page will hold.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct EnvData<M>
where
    M: EnvSpecific,
{
    /// Store the marker type.
    marker: PhantomData<M>,

    /// Set of pressed keys.
    pressed_keys: M::KeySet,
}

impl<M> EnvData<M>
where
    M: EnvSpecific,
{
    /// Check if key is pressed.
    #[inline]
    fn key_pressed(&self, key: &M::Key) -> bool {
        self.pressed_keys.key_set_contains(key)
    }
}

impl<M> Default for EnvData<M>
where
    M: EnvSpecific,
    M::KeySet: Default,
{
    #[inline]
    fn default() -> Self {
        Self {
            marker: PhantomData,
            pressed_keys: M::KeySet::default(),
        }
    }
}

#[derive(Debug)]
pub enum Error {}

/// Helper type alias to get key type for environment.
pub type KeyFor<T> = <T as EnvSpecific>::Key;

/// Helper type alias to get type specifically for environment.
pub type SpecificallyFor<T> = <T as AssociatedEnvSpecificMarker>::EnvSpecificMarker;

#[test]
fn test_keyboard() {
    use crate::aspects::keyboard;
    use crate::env::*;
    use crate::*;

    mod single_aspect {
        use std::collections::HashSet;

        use crate::aspects::keyboard;

        crate::env_for_aspect!(keyboard);

        crate::compose! {
            pub enum Events {
                #[part]
                Aspect(super::Event<EnvSpecificMarker>),
            }
        }

        crate::compose! {
            #[derive(Default)]
            pub struct Page {
                #[part]
                pub aspect: super::EnvData<EnvSpecificMarker>,
                pub time: i16,
            }
        }

        impl keyboard::EnvSpecific for EnvSpecificMarker {
            type Key = u8;

            type KeySet = HashSet<u8>;
        }
    }

    struct App {
        page: single_aspect::Page,
        pressed: bool,
        released: bool,
        repeated: bool,
    }

    impl Sketch for App {
        type Env = single_aspect::Env;

        fn page(&self) -> &single_aspect::Page {
            &self.page
        }

        fn page_mut(&mut self) -> &mut single_aspect::Page {
            &mut self.page
        }

        fn handle_event(&mut self, event: &EventOf<Self::Env>) -> Result<(), Error> {
            handle_event(self, event)
        }
    }

    impl Setup for App {
        fn setup(page: single_aspect::Page, _: ()) -> Self {
            App {
                page,
                pressed: false,
                released: false,
                repeated: false,
            }
        }
    }

    impl Handlers for App {
        fn pressed(&mut self, &key: &u8) {
            assert_eq!(key, 123);
            assert!(self.key_pressed(123));
            self.pressed = true;
        }

        fn repeated(&mut self, &key: &u8) {
            assert_eq!(key, 123);
            assert!(self.key_pressed(123));
            self.repeated = true;
        }

        fn released(&mut self, &key: &u8) {
            assert_eq!(key, 123);
            assert!(!self.key_pressed(123));
            self.released = true;
        }
    }

    let mut mill = single_aspect::Mill;

    let mut app = App::setup(mill.new_page().unwrap(), ());

    // press key
    app.handle_event(&single_aspect::Events::Aspect(keyboard::Event::Press(123))).unwrap();

    // repeat key
    app.handle_event(&single_aspect::Events::Aspect(keyboard::Event::Repeat(123))).unwrap();

    // release key
    app.handle_event(&single_aspect::Events::Aspect(keyboard::Event::Release(
        123,
    ))).unwrap();

    assert!(app.pressed);
    assert!(app.released);
    assert!(app.repeated);
}

//! Minimal environment for use in examples and tests.
//!
//! This environment implements no aspects.

use crate::{env, ream::{Ream, ReamResult}};
use core::convert::Infallible;

/// Minimal events.
///
/// It's not possible to create a value of this type.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Events {}

/// Minimal page.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct Page;

/// Minimal mill.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct Mill;

/// Minimal environment.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub struct Env;

impl env::Environment for Env {
    type Mill = Mill;

    type Return = ();

    type Error = Infallible;

    #[inline]
    fn run<R>(
        self,
        mut ream: R,
    ) -> ReamResult<R, ()>
    where
        R: Ream<Env=Self>,
    {
        while let env::Status::Continue = ream.update(&mut Mill)? {
            core::hint::spin_loop();
        }
        Ok(())
    }
}

impl env::Mill<Infallible> for Mill {
    type Page = Page;

    #[inline]
    fn new_page(&mut self) -> Result<Self::Page, Infallible> {
        Ok(Page::default())
    }
}

impl env::Page<Infallible> for Page {
    type Event = Events;

    #[inline]
    fn start_group(&mut self) -> Result<(), Infallible> { Ok(()) }

    #[inline]
    fn end_group(&mut self) -> Result<(), Infallible> { Ok(()) }

    #[inline]
    fn next_event_in_group(&mut self) -> Option<Self::Event> {
        None
    }

    #[inline]
    fn before_event(&mut self, _event: &Self::Event)  -> Result<(), Infallible> { Ok(()) }

    #[inline]
    fn finish_event(&mut self, _event: Self::Event) -> Result<(), Infallible> { Ok(()) }

    #[inline]
    fn status(&self) -> env::Status {
        env::Status::Stop
    }
}

//! Built-in aspects of environments.
//!
//! Environments implement zero or more aspects to expose their functionality.
//! Aspects are general, but still specific to the environment used.
//! This provides the power of environment specific features, but with a consistent API across
//! environments.
//!
//! Aspects have a `Handlers` trait that allows the user to implement event handlers for
//! the aspect's events. Additionally, each aspect has a `SketchExt` trait that automatically
//! gets implemented for all sketches that have a compatable environment. The `SketchExt` trait
//! provides methods to interact with the environment the sketch is running in.
//!
//! ## Custom Aspects
//!
//! A user or environment can create it's own aspects. The only requirement when creating an aspect
//! is to have a `RunHandlers` with a `run_handlers(&mut self, event)` member. This is needed to be
//! compatable with the [`crate::derive_sketch`] macro. If the aspect doesn't have any associated events
//! then this requirement is not needed.
//!
//! It is recommended to provide an extension trait like `SketchExt` so that the user can call
//! `self.other_method()` instead of needing to call `self.page.other_method()`.

#[cfg(feature = "aspect-update")]
#[cfg_attr(docsrs, doc(cfg(feature = "aspect-update")))]
pub mod update;

#[cfg(feature = "aspect-draw")]
#[cfg_attr(docsrs, doc(cfg(feature = "aspect-draw")))]
pub mod draw;

#[cfg(feature = "aspect-mouse")]
#[cfg_attr(docsrs, doc(cfg(feature = "aspect-mouse")))]
pub mod mouse;

#[cfg(feature = "aspect-keyboard")]
#[cfg_attr(docsrs, doc(cfg(feature = "aspect-keyboard")))]
pub mod keyboard;

//! Interactive visual applications in Rust.
//!
//! ```
//! # #![allow(clippy::needless_doctest_main)]
//! use sketchbook::*;
//! use sketchbook::ream::Single;
//!
//! #[apply(derive_sketch)]
//! #[sketch(
//!     env = minimal,
//!     aspects = (/* aspects go here */),
//! )]
//! struct App {
//!     #[page] page: minimal::Page,
//! }
//!
//! impl Setup for App {
//!     fn setup(page: minimal::Page, _: ()) -> Self {
//!         Self { page }
//!     }
//! }
//!
//! fn main() {
//!     Book::<Single<App>>::bind();
//! }
//! ```
//!
//! ## Environments
//!
//! | Crate                                                       | Description                                                      | Status         |
//! |-------------------------------------------------------------|------------------------------------------------------------------|----------------|
//! | [sketchbook-wgpu](https://crates.io/crates/sketchbook-wgpu) | Directly uses `winit` and `wgpu` to run sketches.                | In Development |
//! | sketchbook-web                                              | Runs sketches in a web browser.                                  | Planned        |
//! | sketchbook-avr                                              | Runs sketches on AVR micro-controllers (E.g. AVR based Arduinos) | Planned        |
//!
//! ## Feature Flags
#![cfg_attr(
    docsrs,
    cfg_attr(doc, doc = ::document_features::document_features!())
)]
// #![warn(missing_docs)]
#![cfg_attr(not(feature = "std"), no_std)]
// only enables the `doc_cfg` feature when
// the `docsrs` configuration attribute is defined
#![cfg_attr(docsrs, feature(doc_cfg))]
#![deny(
    bad_style,
    const_err,
    dead_code,
    improper_ctypes,
    non_shorthand_field_patterns,
    no_mangle_generic_items,
    overflowing_literals,
    path_statements,
    patterns_in_fns_without_body,
    private_in_public,
    unconditional_recursion,
    unused,
    unused_allocation,
    unused_comparisons,
    unused_parens,
    while_true
)]
#![warn(
    rust_2018_idioms,
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    unsafe_code
)]
#![deny(clippy::unwrap_used, clippy::expect_used)]

#[allow(unused_extern_crates)]
#[cfg(feature = "alloc")]
extern crate alloc;

pub mod compose;

mod env;

pub mod ream;

mod book;

mod real;

pub mod aspects;

mod geometry;

pub use book::*;
// pub use ream::*;
pub use env::*;
// pub use aspects::*;
pub use geometry::*;
pub use real::*;

/// Base trait for all sketches.
///
/// A user does not need to interact with this trait directly.
///
/// A sketch is a type that contains a page from an environment.
/// The sketch is then run within the environment and handles the events it generates.
pub trait Sketch {
    /// Environment the sketch can run in.
    type Env: Environment;

    /// Get page for sketch.
    fn page(&self) -> &PageOf<Self::Env>;

    /// Get page mutabily for sketch.
    fn page_mut(&mut self) -> &mut PageOf<Self::Env>;

    /// Handle an event from the environment.
    ///
    /// This is called by the ream the sketch is running in.
    fn handle_event(&mut self, event: &EventOf<Self::Env>) -> Result<(), <Self::Env as Environment>::Error>;
}

/// Allow aspect extension traits to be called directly on `S` instead of using the page field.
impl<C, S> compose::AsPart<C> for S
where
    S: Sketch,
    PageOf<S::Env>: compose::AsPart<C>,
{
    fn as_part(&self) -> &C {
        self.page().as_part()
    }

    fn as_part_mut(&mut self) -> &mut C {
        self.page_mut().as_part_mut()
    }
}

/// Trait for setting up a sketch given some input.
pub trait Setup<Input = ()>
where
    Self: Sketch,
{
    /// Setup sketch using a page from the environment and given input.
    fn setup(page: PageOf<Self::Env>, input: Input) -> Self;
}

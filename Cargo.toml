[package]
name = "sketchbook"
description = "Interactive visual applications in Rust"
version = "0.0.2"
authors = ["Konnor Andrews"]
edition = "2021"
license = "MIT"
keywords = ["graphics", "sketch"]
categories = ["embedded", "graphics", "no-std", "rendering", "visualization"]
readme = "README.md"
repository = "https://gitlab.com/konnorandrews/sketchbook"

[dependencies]
libm = { version="0.2", optional=true }
macro_rules_attribute = { version="0.1", optional=true }
document-features = { version="0.2", optional=true }
palette = { version = "0.6", optional=true }
serde = { version = "1.0", optional=true, features = ["derive"] }

[features]
## No features are enabled by default.
default = []

#! ### Optional Components
#! #### Aspects

## Aspect that periodically updates a sketch.
aspect-update = []
## Aspect for keyboard input.
aspect-keyboard = []
## Aspect for mouse input.
aspect-mouse = []
## Aspect for drawing onto the environment's page.
aspect-draw = []

#! #### Reams

## Ream that runs multiple sketches without threads.
ream-concurrent = ["alloc"]
## Ream that runs multiple sketches on independant threads.
ream-parallel = ["std"]
## Ream that runs a single sketch.
ream-single = []

#! #### Environments

## Macro for generating environments to test aspects.
env-aspect-test = []
## Minimal environment that implements no aspects.
env-minimal = []

#! ### Optional Dependencies

## Support for using the standard library.
std = ["alloc"]
## Support for using allocation.
alloc = []
## Re-export `macro_rules_attribute` for better macro syntax.
derive = ["dep:macro_rules_attribute"]
## Support for `palette` based colors.
color = ["dep:palette"]
## Support for serde.
serde = ["dep:serde", "palette?/serializing"]


# docs.rs-specific configuration
[package.metadata.docs.rs]
# document all features
all-features = true
# defines the configuration attribute `docsrs`
rustdoc-args = ["--cfg", "docsrs"]

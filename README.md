# Sketchbook

Creative coding in rust.

## Notice

This crate is still in early development.
It is fully usable as is, but more features are expected to be added.

## Project Goals

Goals:

- Bring the API style of Processing and p5.js to Rust.
- Be usable by people new to Rust.
- Reduce time from idea to implementation.
- Allow using Sketchbook on any target Rust supports.
- High performance in debug builds.
- Zero dependencies by default.
- Extendable by the user or other crates.
- NO GLOBAL STATE!

Non-goals:

- Perfectly recreate the Processing or p5.js API.
- Support every use case of a graphics library.
- Re-implement functionality from other crates where they could be optional dependencies (i.e. not a monolithic crate).
- Be a framework. All framework like APIs should have a matching individual use version.
- Be itself a graphics backend.

## Environments

Sketchbook uses environments to run sketches in.

| Crate                                                       | Description                                                      | Status         |
|-------------------------------------------------------------|------------------------------------------------------------------|----------------|
| [sketchbook-wgpu](https://crates.io/crates/sketchbook-wgpu) | Directly uses `winit` and `wgpu` to run sketches.                | In Development |
| sketchbook-web                                              | Runs sketches in a web browser.                                  | Planned        |
| sketchbook-avr                                              | Runs sketches on AVR micro-controllers (E.g. AVR based Arduinos) | Planned        |
